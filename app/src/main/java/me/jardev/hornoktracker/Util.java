package me.jardev.hornoktracker;

/**
 * Created by jeba on 27/04/17.
 */
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import java.util.Date;



public class Util {

    public static void savePreference(Activity context, String key, String value){
            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);//context.getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString(key, value);
            boolean commit = editor.commit();
            Log.d("Util", "Data commit: " + commit);

    }

    public static String getPreference(Activity context, String key){
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);//context.getPreferences(Context.MODE_PRIVATE);
        return sharedPref.getString(key,"NA");
    }

    public static void clearPreference(Activity context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();
    }

    public static void showAlert(final Activity context, String title, String message, final boolean closeStatus){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set title
        alertDialogBuilder.setTitle("Stock App- "+title);

        // set dialog message
        alertDialogBuilder
                .setMessage(message)
                .setCancelable(false)
                /*.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // if this button is clicked, close
                        // current activity
                       // Login.this.finish();
                    }
                })*/
                .setNegativeButton("Ok",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        if(closeStatus){
                            context.finish();
                        }
                        dialog.cancel();

                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }
}