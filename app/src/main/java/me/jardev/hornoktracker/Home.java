package me.jardev.hornoktracker;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Home extends AppCompatActivity {

    private EditText userid;
    private Button loginBtn;
    private AppCompatActivity mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        userid = (EditText) findViewById(R.id.userid);
        loginBtn = (Button) findViewById(R.id.loginUser);
        mContext = this;

        if(!Util.getPreference(this,"userid").equals("NA")){


                Intent intent = new Intent(this,MapsActivity.class);
                startActivity(intent);
            }

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e(getLocalClassName(), userid.getText().toString() + "====");
                if (!userid.getText().toString().equalsIgnoreCase("")) {
                   Util.savePreference(mContext,"userid",userid.getText().toString());
                    Intent intent = new Intent(mContext,MapsActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(mContext, "Invalid UserId...", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
