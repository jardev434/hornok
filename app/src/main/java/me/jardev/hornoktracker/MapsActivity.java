package me.jardev.hornoktracker;

/**
 * Created by jeba on 27/04/17.
 */

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.location.Location;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Toast;
import android.os.Handler;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import me.jardev.hornoktracker.mqtt.MQTTservice;

import java.text.DateFormat;
import java.util.Date;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,LocationListener {

    private GoogleMap mMap;
    GPSTracker gps;
    private double lat, longi;
    boolean finegps = false,coarsegps= false;
    private final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 101;
    private final int MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION = 102;
    private String TAG="MAP MapsActivity";
    private final String topic = "HornOK_map123";
    Activity mContext;
    //=========================================================================

    private Messenger service = null;
    private final Messenger serviceHandler = new Messenger(new ServiceHandler());
    private IntentFilter intentFilter = null;
    private PushReceiver pushReceiver;

    //=========================================================================
    private GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Location mCurrentLocation;
    LocationRequest mLocationRequest;
    String mLastUpdateTime;
    boolean mRequestingLocationUpdates = false;

    private Marker userMarker;
    boolean markerAl=false;
    //=========================================================================


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        intentFilter = new IntentFilter();
        intentFilter.addAction("com.tekvillage.mapsample.PushReceived");
        pushReceiver = new PushReceiver();
        registerReceiver(pushReceiver, intentFilter, null, null);
        mContext = this;
        startService(new Intent(this, MQTTservice.class));
        checkPermissions();


        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        if(finegps&&coarsegps) {
            createLocationRequest();
            Log.d(TAG, "Access GRANTED==========>>");

            gps = new GPSTracker(MapsActivity.this);

            lat = gps.getLatitude();
            longi = gps.getLongitude();
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);





        }else{
            Log.d(TAG,"Access denied==========>>");

        }
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.

    }


    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(2000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }
    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(lat, longi);
        userMarker=mMap.addMarker(new MarkerOptions().position(sydney).title("You are here"));
        mMap.setMyLocationEnabled(true);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));



    }


    public void checkPermissions(){
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                ) {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            finegps = false;


        }else{
            finegps = true;
        }
        if(ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION},
                    MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            coarsegps = false;

        }else{
            coarsegps = true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    finegps = true;

                } else {
                    finegps = false;

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            case MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    coarsegps = true;


                } else {
                    coarsegps = false;


                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    @Override
    protected void onStart()
    {
        bindService(new Intent(this, MQTTservice.class), serviceConnection, 0);

        mGoogleApiClient.connect();

        super.onStart();
    }

    @Override
    protected void onStop()
    {
        mGoogleApiClient.disconnect();

        super.onStop();

        unbindService(serviceConnection);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        if(pushReceiver!=null && intentFilter!=null) {
            registerReceiver(pushReceiver, intentFilter);
        }
        if (mGoogleApiClient.isConnected() && !mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        if(pushReceiver!=null && intentFilter!=null) {

            unregisterReceiver(pushReceiver);
        }
        stopLocationUpdates();
    }



    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    @Override
    public void onConnected(Bundle connectionHint) {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        Log.d(TAG, "started location updates==========>>");

        if (mLastLocation != null) {
            // mLatitudeText.setText(String.valueOf(mLastLocation.getLatitude()));
            // mLongitudeText.setText(String.valueOf(mLastLocation.getLongitude()));
        }
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {


        mCurrentLocation = location;
        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());


        LatLng sydney1 = new LatLng(lat, longi);
        LatLng sydney2 = new LatLng(12.0826802, 77.27071840000008);
        LatLng sydney3 = new LatLng(13.0826802, 78.27071840000008);
        LatLng sydney4 = new LatLng(14.0826802, 79.27071840000008);


        //  userMarker.setPosition(sydney);
        mMap.clear();
        //userMarker.remove();
        //  if(!markerAl)
        // {
        Log.d(TAG, "Inside if");


        userMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(location.getLatitude(),location.getLongitude()))
                        .title(Util.getPreference(mContext,"userid"))
                //.icon(BitmapDescriptorFactory.fromResource(R.drawable.taxi)
        );
        mMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(), location.getLongitude())));
        //    userMarker.setPosition(sydney1);
        //  userMarker=mMap.addMarker(new MarkerOptions().position(sydney1).title("updated loc"+lat+"  "+ longi));
        //}
        // userMarker.hideInfoWindow();
        //  userMarker.showInfoWindow();
        //  markerAl = !markerAl;
        //  animateMarker(userMarker,sydney1,false);
        //mMap.addMarker(new MarkerOptions().position(sydney2).title("updated loc"+lat+"  "+ longi));
        // mMap.addMarker(new MarkerOptions().position(sydney3).title("updated loc"+lat+"  "+ longi));
        //mMap.addMarker(new MarkerOptions().position(sydney4).title("updated loc"+lat+"  "+ longi));

        // CameraUpdate cameraUpdate=CameraUpdateFactory.newLatLng(sydney1);
        // mMap.moveCamera(cameraUpdate);
        // mMap.animateCamera(cameraUpdate);
        Log.d(TAG, "onLocationChanged location1 updastes==========>>"+location.getLatitude()+"  "+location.getLongitude());
        publishLocation("ADd:"+lat+","+longi);
    }

    public class PushReceiver extends BroadcastReceiver
    {
        @Override
        public void onReceive(Context context, Intent i)
        {
            String topic = i.getStringExtra(MQTTservice.TOPIC);
            String message = i.getStringExtra(MQTTservice.MESSAGE);
            Toast.makeText(context, "Push message received - " + topic + ":" + message, Toast.LENGTH_LONG).show();

            String msg[] = message.split(":");
            Log.d(TAG,message);

            if(msg.length==2) {
                String user = msg[0];
                String data[] = msg[1].split(",");
                try {
                    double latitude = Double.parseDouble(data[0]);
                    double longitude = Double.parseDouble(data[1]);
                    mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).title(user));
                    mMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(latitude, longitude)));

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    }


    private ServiceConnection serviceConnection = new ServiceConnection()
    {
        @Override
        public void onServiceConnected(ComponentName arg0, IBinder binder)
        {
            service = new Messenger(binder);
            Bundle data = new Bundle();
            //data.putSerializable(MQTTservice.CLASSNAME, MainActivity.class);
            data.putCharSequence(MQTTservice.INTENTNAME, "me.jardev.hornoktracker.PushReceived");
            Message msg = Message.obtain(null, MQTTservice.REGISTER);
            msg.setData(data);
            msg.replyTo = serviceHandler;
            try
            {
                service.send(msg);
            }
            catch (RemoteException e)
            {
                e.printStackTrace();
            }
            initiateSubscription();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0)
        {
            Log.e(TAG,"service disconnected////////");
        }
    };


    class ServiceHandler extends Handler
    {
        @Override
        public void handleMessage(Message msg)
        {
            Log.d(TAG,msg.what+" Service connection "+msg.toString());

            switch (msg.what)
            {
                case MQTTservice.SUBSCRIBE: 	break;
                case MQTTservice.PUBLISH:		break;
                case MQTTservice.REGISTER:		break;
                default:
                    super.handleMessage(msg);
                    return;
            }

            Bundle b = msg.getData();
            if (b != null)
            {
                //TextView result = (TextView) findViewById(R.id.textResultStatus);
                Boolean status = b.getBoolean(MQTTservice.STATUS);
                if (status == false)
                {
                    //result.setText("Fail");
                    Log.d(TAG,"Service connection failed");
                }
                else
                {
                    Log.d(TAG,"Service connection success");
                }
            }
        }
    }

    public void initiateSubscription(){
        Bundle data = new Bundle();
        data.putCharSequence(MQTTservice.TOPIC, topic);
        Message msg = Message.obtain(null, MQTTservice.SUBSCRIBE);
        msg.setData(data);
        msg.replyTo = serviceHandler;
        try
        {
            if(service==null){
                Log.d(TAG,"service is null....");
            }
            if(serviceHandler==null){
                Log.d(TAG,"serviceHandler is null....");
            }
            service.send(msg);
        }
        catch (RemoteException e)
        {
            e.printStackTrace();
            Log.d(TAG,"subscription failed with exception is null...."+e.getMessage());

            // result.setText("Subscribe failed with exception:" + e.getMessage());
        }

    }

    protected void startLocationUpdates() {
        Log.d(TAG, "Starting location updates==========>>");
        if(mGoogleApiClient!=null&& mLocationRequest!=null){
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this);
        }
    }
    protected void stopLocationUpdates() {
        Log.d(TAG, "Stopping location updates==========>>");
        if(mGoogleApiClient!=null && mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this);
        }
    }

    public void animateMarker(final Marker marker, final LatLng toPosition,
                              final boolean hideMarker) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = mMap.getProjection();
        Point startPoint = proj.toScreenLocation(marker.getPosition());
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 500;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);
                double lng = t * toPosition.longitude + (1 - t)
                        * startLatLng.longitude;
                double lat = t * toPosition.latitude + (1 - t)
                        * startLatLng.latitude;
                marker.setPosition(new LatLng(lat, lng));

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else {
                  /*  if (hideMarker) {
                        marker.setVisible(false);
                    } else {
                        marker.setVisible(true);
                    }*/
                }
            }
        });
    }

    public void publishLocation(String message){
        Bundle data = new Bundle();
        data.putCharSequence(MQTTservice.TOPIC, topic);
        data.putCharSequence(MQTTservice.MESSAGE, message);
        Message msg = Message.obtain(null, MQTTservice.PUBLISH);
        msg.setData(data);
        msg.replyTo = serviceHandler;
        try
        {

            service.send(msg);
        }
        catch (RemoteException e)
        {
            e.printStackTrace();
        }
    }
}
