package me.jardev.hornoktracker;

/**
 * Created by jeba on 27/02/16.
 */
public class PermissionNotGrantedException extends Exception {
    public PermissionNotGrantedException() {
    }

    public PermissionNotGrantedException(String detailMessage) {
        super(detailMessage);

    }

    @Override
    public String toString() {
        return "PermissionNotGrantedException{}";
    }
}
